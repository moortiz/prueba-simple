/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WebApplication.Controlador;

import WebApplication.BDatos.ArchivoDAO;
import WebApplication.ValueObjects.ArchivoVO;
import WebApplication.ValueObjects.ArchivosIIVO;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author Miguel
 */
@WebServlet(name = "ControladorArchivo", urlPatterns = {"/ControladorArchivo.jsp"})
public class ControladorArchivo extends HttpServlet {

    int procesa = 0;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        String path = "/";
        String salida = "";
        int opcion = request.getParameter("op") != null ? Integer.parseInt(request.getParameter("op")) : 0;
        switch (opcion) {
            case 0:
                listar_info(request);
                path = "/Archivo.jsp";
                procesa = 2;
                break;
            case 1:
                salida = docunemtos(request, response);
                procesa = 1;
                break;
            case 2:
                request.setAttribute("dato", "doc");
                request.getSession().setAttribute("datos", request.getParameter("datos"));
                path = "/cargar.jsp";
                procesa = 2;
                break;
        }
        switch (procesa) {
            case 1:
                try (PrintWriter out = response.getWriter()) {
                    out.print(salida);
                }
                break;

            case 2:
                System.out.println("llegat inicio 3");
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(path);
                dispatcher.forward(request, response);
                break;
        }
    }

    void listar_info(HttpServletRequest request) throws UnsupportedEncodingException, Exception {
        try {
            request.removeAttribute("archivoVO");
            request.removeAttribute("archivosIIVO");
            List<ArchivoVO> archivoVOs = new ArchivoDAO().Listar();
            List<ArchivosIIVO> archivosIIVOs = new ArchivoDAO().Listar2();
            if (archivoVOs != null || archivosIIVOs != null) {
                request.setAttribute("archivoVO", archivoVOs);
                request.setAttribute("archivosIIVO", archivosIIVOs);
            }
        } catch (Exception e) {
            System.err.println("listar_info" + e.getMessage());
        }
    }

    String docunemtos(HttpServletRequest request, HttpServletResponse response) throws Exception {

        String[] sTexto = {};

        FileItemFactory factory = new DiskFileItemFactory();
        ServletFileUpload upload = new ServletFileUpload(factory);
        List<FileItem> items = upload.parseRequest(request);

        for (FileItem item : items) {
            if (item.getFieldName().compareTo("file-es[]") == 0) {
                System.out.println(item.getString());
                sTexto = item.getString().split("\n");
                System.out.println(sTexto[5].length());
            }
        }
        ArchivoVO archivoVO = new ArchivoVO();
        ArchivosIIVO archivosIIVO = new ArchivosIIVO();
        ArchivoDAO archivoDAO = new ArchivoDAO();
        try {
             for (int i = 0; i < sTexto.length; i++) {
            if(sTexto[i].substring(0, 1) == "01"){
            archivoVO.setINAP_DIGITO_VERIFICA(request.getParameter(sTexto[i].substring(226)));
            archivoVO.setINAP_IDENTIFICACION(request.getParameter(sTexto[i].substring(209,224)));
            archivoVO.setINAP_NOMBRE_RAZONSOCIAL(request.getParameter(sTexto[i].substring(7,206)));
            archivoVO.setINAP_TIPODOCUMENTO(request.getParameter(sTexto[i].substring(207,208)));
            archivoVO.setINPL_MODALIDAD(request.getParameter(sTexto[i].substring(2,2)));
            archivoVO.setINPL_SECUENCIA(request.getParameter(sTexto[i].substring(3,6)));
            archivoVO.setINPL_TIPO_REGISTRO(request.getParameter(sTexto[i].substring(0,1)));
            }else{
            archivosIIVO.setINCO_TIPODOCUMENTO(request.getParameter(sTexto[i].substring(7,8)));
            archivosIIVO.setINCO_IDENTIFICACION(request.getParameter(sTexto[i].substring(9,24)));
            archivosIIVO.setINCO_TIPO(request.getParameter(sTexto[i].substring(25,26)));
            archivosIIVO.setINCO_SUBTIPO(request.getParameter(sTexto[i].substring(27,28)));
            archivosIIVO.setINCO_EXTRANJERO(request.getParameter(sTexto[i].substring(29,29)));
            archivosIIVO.setINCO_COL_RESIDENTE_EXTERIOR(request.getParameter(sTexto[i].substring(30,30)));
            archivosIIVO.setINCO_COD_DEPARTAMENTO(request.getParameter(sTexto[i].substring(31,32)));
            archivosIIVO.setINCO_COD_MUNICIPIO(request.getParameter(sTexto[i].substring(33,35)));
            archivosIIVO.setINCO_APELLIDO1(request.getParameter(sTexto[i].substring(36,55)));
            archivosIIVO.setINCO_APELLIDO2(request.getParameter(sTexto[i].substring(56,85)));
            archivosIIVO.setINCO_NOMBRE1(request.getParameter(sTexto[i].substring(86,105)));
            archivosIIVO.setINCO_NOMBRE2(request.getParameter(sTexto[i].substring(106,136)));
            archivosIIVO.setINPL_MODALIDAD(request.getParameter(sTexto[i].substring(2,2)));
            archivosIIVO.setINPL_SECUENCIA(request.getParameter(sTexto[i].substring(3,6)));
            archivosIIVO.setINPL_TIPO_REGISTRO(request.getParameter(sTexto[i].substring(0,1)));
            }
           archivoDAO.InsertarDetalle(archivosIIVO);
           archivoDAO.InsertarEncabezado(archivoVO);
        }
        } catch (Exception e) {
            System.err.println("error por jdbc");
        }
       

        Map<String, Object> data = new HashMap<String, Object>();
        data.put("success", true);

        Gson gsons = new Gson();
        String jsonString = gsons.toJson(data);
        System.out.println(new Gson().toJson(jsonString));

        return jsonString;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            System.err.println(";ControladorBitacora;doGet;" + ex.getMessage());
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            System.err.println(";ControladorBitacora;doGet;" + ex.getMessage());
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
