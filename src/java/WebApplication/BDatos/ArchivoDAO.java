/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WebApplication.BDatos;

import WebApplication.Conexion.Conexion;
import WebApplication.ValueObjects.ArchivoVO;
import WebApplication.ValueObjects.ArchivosIIVO;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Miguel
 */
public class ArchivoDAO {

    public boolean InsertarEncabezado(ArchivoVO archivoVO) throws SQLException, Exception {
        System.out.println("InsertarDetalle Insertar");
        boolean exito = false;
        CallableStatement cs = null;
        int ind = 1;
        Conexion conexion = new Conexion();
        try {
            cs = conexion.getConeccion().prepareCall("{CALL PRUEBA.PR_DATOS_GEN_APORTANTE_I(?,?,?,?,?,?,?,?)}");
            cs.setString(ind++, archivoVO.getINAP_DIGITO_VERIFICA());
            cs.setString(ind++, archivoVO.getINAP_IDENTIFICACION());
            cs.setString(ind++, archivoVO.getINAP_NOMBRE_RAZONSOCIAL());
            cs.setString(ind++, archivoVO.getINAP_TIPODOCUMENTO());
            cs.setString(ind++, archivoVO.getINPL_MODALIDAD());
            cs.setString(ind++, archivoVO.getINPL_SECUENCIA());
            cs.setString(ind++, archivoVO.getINPL_TIPO_REGISTRO());
            cs.registerOutParameter(ind, 2);
            cs.execute();
            archivoVO.setINPL_ID(String.valueOf(cs.getLong(ind)));
            if (archivoVO.getINPL_ID() != null) {
                exito = true;
            }

        } catch (SQLException ex) {
            System.err.println("InsertarEncabezado ;" + ex.getMessage());
        } finally {
            try {
                cs.close();
                // conexion.desconectar();
            } catch (SQLException ex) {
                System.err.println("InsertarEncabezado ;" + ex.getMessage());
            }
            try {
                conexion.desconectar();
            } catch (SQLException ex) {
                System.err.println("InsertarEncabezado ;" + ex.getMessage());
            }
        }
        return exito;
    }

    public boolean InsertarDetalle(ArchivosIIVO archivosIIVO) throws SQLException, Exception {
        boolean exito = false;
        CallableStatement cs = null;
        int ind = 1;
        Conexion conexion = new Conexion();
        try {
            cs = conexion.getConeccion().prepareCall("{CALL PRUEBA.PR_DESCRIPCION_DETALLADA_I(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
            cs.setString(ind++, archivosIIVO.getINCO_TIPODOCUMENTO());
            cs.setString(ind++, archivosIIVO.getINCO_IDENTIFICACION());
            cs.setString(ind++, archivosIIVO.getINCO_TIPO());
            cs.setString(ind++, archivosIIVO.getINCO_SUBTIPO());
            cs.setString(ind++, archivosIIVO.getINCO_EXTRANJERO());
            cs.setString(ind++, archivosIIVO.getINCO_COL_RESIDENTE_EXTERIOR());
            cs.setString(ind++, archivosIIVO.getINCO_COD_DEPARTAMENTO());
            cs.setString(ind++, archivosIIVO.getINCO_COD_MUNICIPIO());
            cs.setString(ind++, archivosIIVO.getINCO_APELLIDO1());
            cs.setString(ind++, archivosIIVO.getINCO_APELLIDO2());
            cs.setString(ind++, archivosIIVO.getINCO_NOMBRE1());
            cs.setString(ind++, archivosIIVO.getINCO_NOMBRE2());
            cs.setString(ind++, archivosIIVO.getINPL_MODALIDAD());
            cs.setString(ind++, archivosIIVO.getINPL_SECUENCIA());
            cs.setString(ind++, archivosIIVO.getINPL_TIPO_REGISTRO());
            cs.registerOutParameter(ind, 2);
            cs.execute();
            archivosIIVO.setINPL_ID(String.valueOf(cs.getLong(ind)));
            if (archivosIIVO.getINPL_ID() != null) {
                exito = true;
            }

        } catch (SQLException ex) {
            System.err.println(" InsertarDetalle;" + ex.getMessage());
        } finally {
            try {
                cs.close();
                // conexion.desconectar();
            } catch (SQLException ex) {
                System.err.println(" InsertarDetalle;" + ex.getMessage());
            }
            try {
                conexion.desconectar();
            } catch (SQLException ex) {
                System.err.println(" InsertarDetalle;" + ex.getMessage());
            }
        }
        return exito;
    }

    public List<ArchivoVO> Listar() throws Exception, SQLException {
        CallableStatement cs = null;
        ResultSet rs = null;
        StringBuilder sql = new StringBuilder();
        Conexion conexion = new Conexion();
        List<ArchivoVO> lista = null;
        ArchivoVO archivoVO = null;
        try {
            cs = conexion.getConeccion().prepareCall("BEGIN PR_DATOS_GEN_APORTANTE_C (?); END;");
            cs.registerOutParameter(1, -10);
            cs.executeQuery();
            rs = (ResultSet) cs.getObject(1);
            if (rs.isBeforeFirst()) {
                lista = new ArrayList();
                for (; rs.next(); lista.add(archivoVO)) {
                    archivoVO = new ArchivoVO();
                    archivoVO.setINAP_TIPODOCUMENTO(rs.getString("INAP_TIPODOCUMENTO"));
                    archivoVO.setINAP_IDENTIFICACION(rs.getString("INAP_IDENTIFICACION"));
                    archivoVO.setINAP_NOMBRE_RAZONSOCIAL(rs.getString("INAP_NOMBRE_RAZONSOCIAL"));
                    archivoVO.setINAP_DIGITO_VERIFICA(rs.getString("INAP_DIGITO_VERIFICA"));
                    archivoVO.setINPL_ID(rs.getString("INPL_ID"));
                    archivoVO.setINPL_TIPO_REGISTRO(rs.getString("INPL_TIPO_REGISTRO"));
                    archivoVO.setINPL_MODALIDAD(rs.getString("INPL_MODALIDAD"));
                    archivoVO.setINPL_SECUENCIA(rs.getString("INPL_SECUENCIA"));
                 }
            }
        } catch (SQLException ex) {
            System.err.println(" Listar2;" + ex.getMessage());
        } finally {
            try {
//                ps.close();
                cs.close();
            } catch (SQLException ex) {
                System.err.println(" Listar2;" + ex.getMessage());
            }
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.err.println(" Listar2;" + ex.getMessage());
            }
            try {
                if (conexion != null) {
                    conexion.desconectar();
                }
            } catch (SQLException ex) {
                System.err.println(" Listar2;" + ex.getMessage());
            }
        }
        return lista;
    }
    
     public List<ArchivosIIVO> Listar2() throws Exception, SQLException {
        CallableStatement cs = null;
        ResultSet rs = null;
        StringBuilder sql = new StringBuilder();
        Conexion conexion = new Conexion();
        List<ArchivosIIVO> lista = null;
        ArchivosIIVO archivosVO = null;
        try {
            cs = conexion.getConeccion().prepareCall("BEGIN PR_DESCRIPCION_DETALLADA_C (?); END;");
            cs.registerOutParameter(1, -10);
            cs.executeQuery();
            rs = (ResultSet) cs.getObject(1);
            if (rs.isBeforeFirst()) {
                lista = new ArrayList();
                for (; rs.next(); lista.add(archivosVO)) {
                    archivosVO = new ArchivosIIVO();
                    archivosVO.setINCO_TIPODOCUMENTO(rs.getString("INCO_TIPODOCUMENTO"));
                    archivosVO.setINCO_IDENTIFICACION(rs.getString("INCO_IDENTIFICACION"));
                    archivosVO.setINCO_TIPO(rs.getString("INCO_TIPO"));
                    archivosVO.setINCO_SUBTIPO(rs.getString("INCO_SUBTIPO"));
                    archivosVO.setINCO_EXTRANJERO(rs.getString("INCO_EXTRANJERO"));
                    archivosVO.setINCO_COL_RESIDENTE_EXTERIOR(rs.getString("INCO_COL_RESIDENTE_EXTERIOR"));
                    archivosVO.setINCO_COD_DEPARTAMENTO(rs.getString("INCO_COD_DEPARTAMENTO"));
                    archivosVO.setINCO_COD_MUNICIPIO(rs.getString("INCO_COD_MUNICIPIO"));
                    archivosVO.setINCO_APELLIDO1(rs.getString("INCO_APELLIDO1"));
                    archivosVO.setINCO_APELLIDO2(rs.getString("INCO_APELLIDO2"));
                    archivosVO.setINCO_NOMBRE1(rs.getString("INCO_NOMBRE1"));
                    archivosVO.setINCO_NOMBRE2(rs.getString("INCO_NOMBRE2"));
                    archivosVO.setINPL_ID(rs.getString("INPL_ID"));
                    archivosVO.setINPL_TIPO_REGISTRO(rs.getString("INPL_TIPO_REGISTRO"));
                    archivosVO.setINPL_MODALIDAD(rs.getString("INPL_MODALIDAD"));
                    archivosVO.setINPL_SECUENCIA(rs.getString("INPL_SECUENCIA"));
                 }
            }
        } catch (SQLException ex) {
            System.err.println(" Listar2;" + ex.getMessage());
        } finally {
            try {
//                ps.close();
                cs.close();
            } catch (SQLException ex) {
                System.err.println(" Listar2;" + ex.getMessage());
            }
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.err.println(" Listar2;" + ex.getMessage());
            }
            try {
                if (conexion != null) {
                    conexion.desconectar();
                }
            } catch (SQLException ex) {
                System.err.println(" Listar2;" + ex.getMessage());
            }
        }
        return lista;
    }
}
