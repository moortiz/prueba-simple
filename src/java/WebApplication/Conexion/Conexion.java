/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WebApplication.Conexion;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.sql.DataSource;

/**
 *
 * @author Miguel
 */

public class Conexion {

 
    static String bd = "jdbc/Archivos";
    Connection connection = null;


    public Connection getConeccion() {
        return connection;
    }

    public void desconectar() throws SQLException, Exception {
        connection.close();
        connection = null;
            System.out.println(";;Conexion;Desconexión a base de datos ;Desconexión");
    }

    public Conexion() throws SQLException, Exception{
        try {
            InitialContext context = new InitialContext();
            DataSource datasource = (DataSource) context.lookup(bd);
            connection = datasource.getConnection();
        } catch (Exception ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(";;Conexion;error de conexion "+ bd +";Error en Conexion;" + ex.getMessage());
        }
    }
}

