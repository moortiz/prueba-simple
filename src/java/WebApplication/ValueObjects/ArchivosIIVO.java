/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WebApplication.ValueObjects;

/**
 *
 * @author Miguel
 */
public class ArchivosIIVO {

    private String INCO_TIPODOCUMENTO;
    private String INCO_IDENTIFICACION;
    private String INCO_TIPO;
    private String INCO_SUBTIPO;
    private String INCO_EXTRANJERO;
    private String INCO_COL_RESIDENTE_EXTERIOR;
    private String INCO_COD_DEPARTAMENTO;
    private String INCO_COD_MUNICIPIO;
    private String INCO_APELLIDO1;
    private String INCO_APELLIDO2;
    private String INCO_NOMBRE1;
    private String INCO_NOMBRE2;

    private String INPL_ID;
    private String INPL_TIPO_REGISTRO;
    private String INPL_MODALIDAD;
    private String INPL_SECUENCIA;

    public String getINCO_TIPODOCUMENTO() {
        return INCO_TIPODOCUMENTO;
    }

    public void setINCO_TIPODOCUMENTO(String INCO_TIPODOCUMENTO) {
        this.INCO_TIPODOCUMENTO = INCO_TIPODOCUMENTO;
    }

    public String getINCO_IDENTIFICACION() {
        return INCO_IDENTIFICACION;
    }

    public void setINCO_IDENTIFICACION(String INCO_IDENTIFICACION) {
        this.INCO_IDENTIFICACION = INCO_IDENTIFICACION;
    }

    public String getINCO_TIPO() {
        return INCO_TIPO;
    }

    public void setINCO_TIPO(String INCO_TIPO) {
        this.INCO_TIPO = INCO_TIPO;
    }

    public String getINCO_SUBTIPO() {
        return INCO_SUBTIPO;
    }

    public void setINCO_SUBTIPO(String INCO_SUBTIPO) {
        this.INCO_SUBTIPO = INCO_SUBTIPO;
    }

    public String getINCO_EXTRANJERO() {
        return INCO_EXTRANJERO;
    }

    public void setINCO_EXTRANJERO(String INCO_EXTRANJERO) {
        this.INCO_EXTRANJERO = INCO_EXTRANJERO;
    }

    public String getINCO_COL_RESIDENTE_EXTERIOR() {
        return INCO_COL_RESIDENTE_EXTERIOR;
    }

    public void setINCO_COL_RESIDENTE_EXTERIOR(String INCO_COL_RESIDENTE_EXTERIOR) {
        this.INCO_COL_RESIDENTE_EXTERIOR = INCO_COL_RESIDENTE_EXTERIOR;
    }

    public String getINCO_COD_DEPARTAMENTO() {
        return INCO_COD_DEPARTAMENTO;
    }

    public void setINCO_COD_DEPARTAMENTO(String INCO_COD_DEPARTAMENTO) {
        this.INCO_COD_DEPARTAMENTO = INCO_COD_DEPARTAMENTO;
    }

    public String getINCO_COD_MUNICIPIO() {
        return INCO_COD_MUNICIPIO;
    }

    public void setINCO_COD_MUNICIPIO(String INCO_COD_MUNICIPIO) {
        this.INCO_COD_MUNICIPIO = INCO_COD_MUNICIPIO;
    }

    public String getINCO_APELLIDO1() {
        return INCO_APELLIDO1;
    }

    public void setINCO_APELLIDO1(String INCO_APELLIDO1) {
        this.INCO_APELLIDO1 = INCO_APELLIDO1;
    }

    public String getINCO_APELLIDO2() {
        return INCO_APELLIDO2;
    }

    public void setINCO_APELLIDO2(String INCO_APELLIDO2) {
        this.INCO_APELLIDO2 = INCO_APELLIDO2;
    }

    public String getINCO_NOMBRE1() {
        return INCO_NOMBRE1;
    }

    public void setINCO_NOMBRE1(String INCO_NOMBRE1) {
        this.INCO_NOMBRE1 = INCO_NOMBRE1;
    }

    public String getINCO_NOMBRE2() {
        return INCO_NOMBRE2;
    }

    public void setINCO_NOMBRE2(String INCO_NOMBRE2) {
        this.INCO_NOMBRE2 = INCO_NOMBRE2;
    }

    public String getINPL_ID() {
        return INPL_ID;
    }

    public void setINPL_ID(String INPL_ID) {
        this.INPL_ID = INPL_ID;
    }

    public String getINPL_TIPO_REGISTRO() {
        return INPL_TIPO_REGISTRO;
    }

    public void setINPL_TIPO_REGISTRO(String INPL_TIPO_REGISTRO) {
        this.INPL_TIPO_REGISTRO = INPL_TIPO_REGISTRO;
    }

    public String getINPL_MODALIDAD() {
        return INPL_MODALIDAD;
    }

    public void setINPL_MODALIDAD(String INPL_MODALIDAD) {
        this.INPL_MODALIDAD = INPL_MODALIDAD;
    }

    public String getINPL_SECUENCIA() {
        return INPL_SECUENCIA;
    }

    public void setINPL_SECUENCIA(String INPL_SECUENCIA) {
        this.INPL_SECUENCIA = INPL_SECUENCIA;
    }
    
    

}
