/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WebApplication.ValueObjects;

/**
 *
 * @author Miguel
 */
public class ArchivoVO {
    
    private String INAP_TIPODOCUMENTO;
    private String INAP_IDENTIFICACION;
    private String INAP_NOMBRE_RAZONSOCIAL;
    private String INAP_DIGITO_VERIFICA;
    
    private String INPL_ID;
    private String INPL_TIPO_REGISTRO;
    private String INPL_MODALIDAD;
    private String INPL_SECUENCIA;

    public String getINAP_TIPODOCUMENTO() {
        return INAP_TIPODOCUMENTO;
    }

    public void setINAP_TIPODOCUMENTO(String INAP_TIPODOCUMENTO) {
        this.INAP_TIPODOCUMENTO = INAP_TIPODOCUMENTO;
    }

    public String getINAP_IDENTIFICACION() {
        return INAP_IDENTIFICACION;
    }

    public void setINAP_IDENTIFICACION(String INAP_IDENTIFICACION) {
        this.INAP_IDENTIFICACION = INAP_IDENTIFICACION;
    }

    public String getINAP_NOMBRE_RAZONSOCIAL() {
        return INAP_NOMBRE_RAZONSOCIAL;
    }

    public void setINAP_NOMBRE_RAZONSOCIAL(String INAP_NOMBRE_RAZONSOCIAL) {
        this.INAP_NOMBRE_RAZONSOCIAL = INAP_NOMBRE_RAZONSOCIAL;
    }

    public String getINAP_DIGITO_VERIFICA() {
        return INAP_DIGITO_VERIFICA;
    }

    public void setINAP_DIGITO_VERIFICA(String INAP_DIGITO_VERIFICA) {
        this.INAP_DIGITO_VERIFICA = INAP_DIGITO_VERIFICA;
    }

    public String getINPL_ID() {
        return INPL_ID;
    }

    public void setINPL_ID(String INPL_ID) {
        this.INPL_ID = INPL_ID;
    }

    public String getINPL_TIPO_REGISTRO() {
        return INPL_TIPO_REGISTRO;
    }

    public void setINPL_TIPO_REGISTRO(String INPL_TIPO_REGISTRO) {
        this.INPL_TIPO_REGISTRO = INPL_TIPO_REGISTRO;
    }

    public String getINPL_MODALIDAD() {
        return INPL_MODALIDAD;
    }

    public void setINPL_MODALIDAD(String INPL_MODALIDAD) {
        this.INPL_MODALIDAD = INPL_MODALIDAD;
    }

    public String getINPL_SECUENCIA() {
        return INPL_SECUENCIA;
    }

    public void setINPL_SECUENCIA(String INPL_SECUENCIA) {
        this.INPL_SECUENCIA = INPL_SECUENCIA;
    }
    
            
            
}
