<%-- 
    Document   : cargar
    Created on : 5/05/2020, 10:39:10 PM
    Author     : Miguel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<c:if test="${dato eq 'doc'}">
    <link href="plugins/bootstrap-fileinput-master/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
    <!--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" crossorigin="anonymous">-->
    <link href="plugins/bootstrap-fileinput-master/themes/explorer-fas/theme.css" media="all" rel="stylesheet" type="text/css"/>
    <script src="plugins/bootstrap-fileinput-master/js/plugins/piexif.js" type="text/javascript"></script>
    <script src="plugins/bootstrap-fileinput-master/js/plugins/sortable.js" type="text/javascript"></script>
    <script src="plugins/bootstrap-fileinput-master/js/fileinput.js" type="text/javascript"></script>
    <script src="plugins/bootstrap-fileinput-master/js/locales/es.js" type="text/javascript"></script>
    <script src="plugins/bootstrap-fileinput-master/themes/fas/theme.js" type="text/javascript"></script>
    <script src="plugins/bootstrap-fileinput-master/themes/explorer-fas/theme.js" type="text/javascript"></script>
                 <div class="container my-4">
        <!--<form enctype="multipart/form-data">-->
        <div class="file-loading">
            <label for="file-es" role="button">Seleccionar Archivos</label>
            <input id="file-es" name="file-es[]" type="file" multiple>
            <small class="form-text text-muted">Seleccionar archivos.</small>
        </div> 
        <div id="kv-error-2" style="margin-top:10px;display:none"></div>
        <div id="kv-success-2" class="alert alert-success" style="margin-top:10px;display:none"></div>

        <!--</form>-->    
    </div> 
    <script>
        $(document).ready(function () {
        $("#file-es").fileinput({
            language: 'es',
            uploadAsync: false,
            overwriteInitial: false,
            removeFromPreviewOnError: true,
            uploadUrl: "ControladorArchivo.jsp?op=1",
            hideThumbnailContent: true, // hide image, pdf, text or other content in the thumbnail preview
            maxFileCount: 1,
            elErrorContainer: '#kv-error-2'
        }).on('filebatchpreupload', function (event, data, id, index) {
            $('#kv-success-2').html('<h4>Estado de la Carga</h4><ul></ul>').hide();
        }).on('filebatchuploadsuccess', function (event, data) {
            var out = '';
            $.each(data.files, function (key, file) {
            var fname = file.name;
            out = out + '<li>' + 'Archivo Cargado # ' + (key + 1) + ' - ' + fname + ' Correcto.' + '</li>';
            cerrarModal();    
            direccionarlink('ControladorArchivo.jsp?op=0');
            });
        $('#kv-success-2 ul').append(out);
        $('#kv-success-2').fadeIn('slow');
        });
        
        }); 
    </script>
</c:if>
