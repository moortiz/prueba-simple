-- Generado por Oracle SQL Developer Data Modeler 19.4.0.350.1424
--   en:        2020-05-06 03:23:02 COT
--   sitio:      Oracle Database 11g
--   tipo:      Oracle Database 11g



CREATE TABLE informacionaportante (
    inap_tipodocumento       VARCHAR2(2 BYTE),
    inap_identificacion      NUMBER(15) NOT NULL,
    inap_nombre_razonsocial  VARCHAR2(199 BYTE),
    inap_digito_verifica     NUMBER(1)
)
LOGGING;

ALTER TABLE informacionaportante ADD CONSTRAINT informacionaportante_pk PRIMARY KEY ( inap_identificacion );

CREATE TABLE informacioncotizante (
    inco_tipodocumento           NUMBER(2),
    inco_identificacion          NUMBER(15) NOT NULL,
    inco_tipo                    VARCHAR2(2 BYTE),
    inco_subtipo                 VARCHAR2(2 BYTE),
    inco_extranjero              VARCHAR2(1 BYTE),
    inco_col_residente_exterior  VARCHAR2(1 BYTE),
    inco_cod_departamento        NUMBER(3),
    inco_cod_municipio           NUMBER(3),
    inco_apellido1               VARCHAR2(29 BYTE),
    inco_apellido2               VARCHAR2(29 BYTE),
    inco_nombre1                 VARCHAR2(29 BYTE),
    inco_nombre2                 VARCHAR2(29 BYTE)
)
LOGGING;

ALTER TABLE informacioncotizante ADD CONSTRAINT informacioncotizante_pk PRIMARY KEY ( inco_identificacion );

CREATE TABLE informacionplanilla (
    inpl_id             NUMBER(20) NOT NULL,
    inco_id             NUMBER(20) NOT NULL,
    inap_id             NUMBER(20) NOT NULL,
    inpl_tipo_registro  VARCHAR2(2 BYTE),
    inpl_modalidad      VARCHAR2(1 BYTE),
    inpl_secuencia      VARCHAR2(4 BYTE)
)
LOGGING;

ALTER TABLE informacionplanilla ADD CONSTRAINT informacionplanilla_pk PRIMARY KEY ( inpl_id );

ALTER TABLE informacionplanilla
    ADD CONSTRAINT infoplanilla_infoaportante_fk FOREIGN KEY ( inap_id )
        REFERENCES informacionaportante ( inap_identificacion )
    NOT DEFERRABLE;

ALTER TABLE informacionplanilla
    ADD CONSTRAINT infoplanilla_infocotizante_fk FOREIGN KEY ( inco_id )
        REFERENCES informacioncotizante ( inco_identificacion )
    NOT DEFERRABLE;

CREATE SEQUENCE s_inpl_id START WITH 1 NOCACHE ORDER;

CREATE OR REPLACE TRIGGER tri_inpl_id BEFORE
    INSERT ON informacionplanilla
    FOR EACH ROW
    WHEN ( new.inpl_id IS NULL )
BEGIN
    :new.inpl_id := s_inpl_id.nextval;
END;
/



-- Informe de Resumen de Oracle SQL Developer Data Modeler: 
-- 
-- CREATE TABLE                             3
-- CREATE INDEX                             0
-- ALTER TABLE                              5
-- CREATE VIEW                              0
-- ALTER VIEW                               0
-- CREATE PACKAGE                           0
-- CREATE PACKAGE BODY                      0
-- CREATE PROCEDURE                         0
-- CREATE FUNCTION                          0
-- CREATE TRIGGER                           1
-- ALTER TRIGGER                            0
-- CREATE COLLECTION TYPE                   0
-- CREATE STRUCTURED TYPE                   0
-- CREATE STRUCTURED TYPE BODY              0
-- CREATE CLUSTER                           0
-- CREATE CONTEXT                           0
-- CREATE DATABASE                          0
-- CREATE DIMENSION                         0
-- CREATE DIRECTORY                         0
-- CREATE DISK GROUP                        0
-- CREATE ROLE                              0
-- CREATE ROLLBACK SEGMENT                  0
-- CREATE SEQUENCE                          1
-- CREATE MATERIALIZED VIEW                 0
-- CREATE MATERIALIZED VIEW LOG             0
-- CREATE SYNONYM                           0
-- CREATE TABLESPACE                        0
-- CREATE USER                              0
-- 
-- DROP TABLESPACE                          0
-- DROP DATABASE                            0
-- 
-- REDACTION POLICY                         0
-- 
-- ORDS DROP SCHEMA                         0
-- ORDS ENABLE SCHEMA                       0
-- ORDS ENABLE OBJECT                       0
-- 
-- ERRORS                                   0
-- WARNINGS                                 0
