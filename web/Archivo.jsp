<%-- 
    Document   : Archivo
    Created on : 5/05/2020, 10:01:40 PM
    Author     : Miguel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="plugins/DataTables/media/css/dataTables.bootstrap.min.css">
        <link rel="stylesheet" href="plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css">
        <link href="plugins/datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
        <link href="plugins/bootstrap/css/rowGroup.bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/entorno.css?<%=Math.random()%>" rel="stylesheet" />
        <link href="css/style_base.css?<%=Math.random()%>" rel="stylesheet" />

        <script src="plugins/jquery/jquery-2.2.2.min.js"></script>
        <script src="plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="plugins/jquery/jquery.validate.min.js" type="text/javascript"></script>
        <script src="plugins/DataTables/media/js/jquery.dataTables.js"></script>
        <script src="plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
        <script src="plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
        <script src="plugins/DataTables/extensions/Responsive/js/responsive.bootstrap.min.js"></script>
        <script src="plugins/datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
        <script src="plugins/datetimepicker/js/locales/bootstrap-datetimepicker.es.js" type="text/javascript"></script>
        <script src="plugins/bootstrap/js/rowGroup.bootstrap.min.js" type="text/javascript"></script>

        <script src="plugins/bootbox/bootbox.min.js"></script>

        <script src="js/base.js?<%=Math.random()%>"></script>
        <script src="js/aplicacion.js?<%=Math.random()%>"></script>
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


        <title>JSP Page</title>
    </head>
    <body class="border border-primary">
        <div id="contenidoInterno" style="width: 100%">
            <div class="contenedor">


                <h1>Encabezado</h1>
                <div class="table-responsive text-nowrap">
                    <table id="example" class="table table-striped table-bordered" style="width:100%;">
                        <thead>
                            <tr>
                                <th style="text-align: center;">#</th>
                                <th style="text-align: center;">Tipo de Registro</th>
                                <th style="text-align: center;">Modalidad de la Planilla</th>
                                <th style="text-align: center;">Secuencia</th>
                                <th style="text-align: center;">Nombre o Razón Social del Aportante</th>
                                <th style="text-align: center;">Tipo Documento del aportante</th>
                                <th style="text-align: center;">Nº de Ident if icación Aportante</th>
                                <th style="text-align: center;">Digito de Verif icación</th>

                            </tr>
                        </thead>
                        <tbody style="text-align: center">
                            <c:forEach var="archivoVO" items="${archivoVO}"  varStatus="i">
                                <tr>
                                    <td>${i}</td>
                                    <td>${archivoVO.INPL_TIPO_REGISTRO}</td>
                                    <td>${archivoVO.INPL_MODALIDAD}</td>
                                    <td>${archivoVO.INPL_SECUENCIA}</td>
                                    <td>${archivoVO.INAP_NOMBRE_RAZONSOCIAL}</td>
                                    <td>${archivoVO.INAP_TIPODOCUMENTO}</td>
                                    <td>${archivoVO.INAP_IDENTIFICACION}</td>
                                    <td>${archivoVO.INAP_DIGITO_VERIFICA}</td>
                                </tr>
                            </c:forEach>

                        </tbody>
                    </table>
                    
                </div>
                <script>
                    $(document).ready(function () {
                        $('#example').DataTable();
                    });
                </script>

                <h1>Detalle</h1>
                <div class="table-responsive text-nowrap">
                    <table id="example2" class="table table-striped table-bordered" style="width:100%;">
                        <thead>
                            <tr>
                                <th style="text-align: center;">#</th>
                                <th style="text-align: center;">Tipo de Registro</th>
                                <th style="text-align: center;">Modalidad de la Planilla</th>
                                <th style="text-align: center;">Secuencia</th>
                                <th style="text-align: center;">Tipo de Documento</th>
                                <th style="text-align: center;">Nº de Identificación Cotizante</th>
                                <th style="text-align: center;">Tipo de Cotizante</th>
                                <th style="text-align: center;">Subtipo de Cotizante</th>
                                <th style="text-align: center;">Extranjero no Obligado a Cotizar Pensiones</th>
                                <th style="text-align: center;">Colombiano Residente en el Exterior</th>
                                <th style="text-align: center;">Código de Departamento</th>
                                <th style="text-align: center;">Código de Municipio</th>
                                <th style="text-align: center;">1º Apellido</th>
                                <th style="text-align: center;">2º Apellido</th>
                                <th style="text-align: center;">1º nombre</th>
                                <th style="text-align: center;">2º Nombre</th>
                            </tr>
                        </thead>
                        <tbody style="text-align: center">
                            <c:forEach var="archivosIIVO" items="${archivosIIVO}"  varStatus="i">
                                <tr>
                                    <td>${i}</td>
                                    <td>${archivosIIVO.INPL_TIPO_REGISTRO}</td>
                                    <td>${archivosIIVO.INPL_MODALIDAD}</td>
                                    <td>${archivosIIVO.INPL_SECUENCIA}</td>
                                    <td>${archivosIIVO.INCO_TIPODOCUMENTO}</td>
                                    <td>${archivosIIVO.INCO_IDENTIFICACION}</td>
                                    <td>${archivosIIVO.INCO_TIPO}</td>
                                    <td>${archivosIIVO.INCO_SUBTIPO}</td>
                                    <td>${archivosIIVO.INCO_EXTRANJERO}</td>
                                    <td>${archivosIIVO.INCO_COL_RESIDENTE_EXTERIOR}</td>
                                    <td>${archivosIIVO.INCO_COD_DEPARTAMENTO}</td>
                                    <td>${archivosIIVO.INCO_COD_MUNICIPIO}</td>
                                    <td>${archivosIIVO.INCO_APELLIDO1}</td>
                                    <td>${archivosIIVO.INCO_APELLIDO2}</td>
                                    <td>${archivosIIVO.INCO_NOMBRE1}</td>
                                    <td>${archivosIIVO.INCO_NOMBRE2}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
        
                </div>
                <br>
                 <input type="button" value="Agregar" class="btn btn-info" onclick="ventanaModal('ControladorArchivo.jsp', 'op=2', 'Agragar Documentos', 'large')" />
                <script>
                    $(document).ready(function () {
                        $('#example2').DataTable();
                    });
                </script>
            </div>
        </div>

    </body>
</html>
