/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var heigth = $(window).height();
var width = $(window).width();
var listtabs = {};
var openNavbar = width > 750 ? true : false;
var activeModals = new Array();

$(document).ajaxStart(function () {
    $('.overlay').show();
});

$(document).ajaxStop(function () {
    setTimeout(function () {
        $('.overlay').hide();
    }, 300);
});


function peticion(controlador, datos) {
    $.ajax({
        url: controlador,
        type: "POST",
        data:  datos,
        success: function (r) {

            var tmpFunc = new Function(r);
            tmpFunc();
//            eval(r);
        },
        timeout: 30000,
        error: function () {
            alertify.alert("Error!!", "Error en el procesamiento");
        }
    });
}

function enviarCTR(id, url) {
    if (url != "") {
        $.ajax({
            url: url,
            type: "POST",
            data: "",
            success: function (datos) {
                $("#" + id).html("");
                $("#" + id).append(datos);
//                $("#mensaje").dialog("destroy");
                $("#mensaje").remove();
            },
            error: function () {
                alertify.alert("Error!!", "Error en el procesamiento");
            }
        });
    }
}

function enviarCTRFrom(id, url) {
    if (url != "") {
        var cadenaFormulario = $('#FormAplication1').serialize();
        $.ajax({
            url: url,
            type: "POST",
            data: cadenaFormulario,
            success: function (datos) {
                $("#" + id).html("");
                $("#" + id).append(datos);
//                $("#mensaje").dialog("destroy");
                $("#mensaje").remove();
                cerrarModal();
            },
            error: function () {
                alertify.alert("Error!!", "Error en el procesamiento");
            }
        });
    }
}

function peticionArchivo(controlador, idForm) {
    var formData = new FormData(document.getElementById(idForm));
    $.ajax({
        url: controlador,
        type: "POST",
        data: formData,
        //dataType: "xml",
        async: true,
        contentType: false,
        processData: false,
        success: function (r) {
            eval(r);
        },
        timeout: 30000,
        error: function () {
            alert("Error en el procesamiento");
        }
    });
}

function enviarFormulario(controlador, idForm) {
    if ($('#' + idForm).parsley().isValid()) {
        peticion(controlador, $('#' + idForm).serialize());
    } else {
        $('#' + idForm).submit();
    }
}

function enviarFormularioArchivo(controlador, idForm) {
    if ($('#' + idForm).parsley().isValid()) {
        peticionArchivo(controlador, idForm);
    } else {
        $('#' + idForm).submit();
    }
}

function recargarpanel(url) {
    recargarpanelDato(url, '');
}

function recargarpanelDato(url, dato) {
    $("#" + getActiveTab()).load(url + dato);

}

//var activeModals = [];
/**
 * 
 * @param {type} url -ubicacion de la pag a mostrar
 * @param {type} datos -- parametros adicionales que se nesesiten enviar
 * @param {type} titulo -- titulo de la modal
 * @param {type} tamaño -- tamaño del dialogo (large, small, null)
 * @returns {undefined}
 */

function ventanaModal(url, datos, titulo, tamaño) {

    //var next = modalesActivas.length;
    var next = activeModals.length;

    // console.log(next);
    var dialog;
    $.ajax({
        url: url, // url a cargar en el dialogo
        data: datos, // datos necesarios para esa url
        cache: false
    })
            .done(function (html) {
                activeModals[next] = bootbox.dialog({
//                bootbox.dialog({
                    message: html,
                    title: titulo, // titulo del dialogo
                    size: tamaño, // tamaño del dialogo (large, small, null)
                    onEscape: true
                });
            });

}

function cerrarModal() {
    var x = $(document).find(' div.bootbox.modal.fade.in');
    $(x[x.length - 1]).modal("hide");

}

function cerrarTodasModal() {
    bootbox.hideAll();

}

function ventanaNueva(url) {
    var aw = screen.availWidth;
    var ah = screen.availHeight;
    var opciones = "width=" + aw + ", height=" + ah + ", screenX=0, screenY=0, top=0, left=0, scrollbars=no, status=no, resizable=no";
    mi_ventana = window.open(url, "", opciones);
}

function toogleHeader() {
    $('#ddd').toggleClass('hederOcu');
    $('#ddd1').toggleClass('hidden');
    $('#main1').toggleClass('closeHeader');
    $('#Sidenav').toggleClass('closeHeader');
//    #main1.closeHeader,#Sidenav.closeHeader {
}

function toogleNav() {
    $('#Sidenav').toggleClass('sidenavOcu');
    $('#main1').toggleClass('closeNav');
}

function buscarTab(cod) {
    var x = -1;
    $(".tabPanels a").each(function (index) {
        if ("#tab" + cod === $(this).attr("href")) {
            x = index;
            $(this).tab('show');
            return x;
        }
    });
    return x;
}

var tabId = 1;

function addTab(titulo, url, cod) {
    if (buscarTab(cod) < 0) {
        tabId++;
        var panel = $('<li class="tabPanels " ><a onclick="viewTab(this)" id="tab' + tabId + '" href="#tab' + cod + '" role="tab" data="tab" >' + titulo + '<button class="close" type="button" title="Remover esta pestaña" onclick="removeTab(this)">×</button></a></li>');
        $('#tab-list').append(panel);
        $('#tab-content').append($('<div class="tab-pane fade " id="tab' + cod + '">No se cargo el panel...</div>'));
        $("#tab" + cod).load(url);
        $("#tab" + tabId).tab('show');
        $('[data-toggle="tooltip"]').tooltip({placement: "bottom"});
    }
}

function getActiveTab() {
    return  $('#tab-content').find('.tab-pane.active').attr("id");
}

function removeTab(x) {

    var tabID = $(x).parents('a').attr('href');
    $(x).parent().prev().addClass("active");
    $(x).parents('li').remove();
    $(tabID).remove();

    //display first tab
    var tabFirst = $('#tab-list a:first');
    tabFirst.tab('show');
    scrollrigth();
}

function viewTab(x) {
    $(x).tab('show');
}
////SCROLL
//var hidWidth;
//var scrollBarWidths = 40;
//var currentPosition = 0;
//
//var widthOfList = function () {
//    var itemsWidth = 0;
//    $('.list li').each(function () {
//        var itemWidth = $(this).outerWidth();
//        itemsWidth += itemWidth;
//    });
//    return itemsWidth;
//};
//
//var widthOfHidden = function () {
//    return ($('.wrapper').outerWidth());
////    return 200;
//};
//
//var getLeftPosi = function () {
//    return $('.list').position().left;
//};
//
//var getRigthPosi = function () {
//    return widthOfList() + getLeftPosi();
//};
//
//function scrollrigth() {
////    console.log(":::::");
////    console.log(getLeftPosi());
////    console.log(getRigthPosi());
////    console.log(widthOfList());
////    console.log(widthOfHidden());
////    console.log(":::::");
//    if (getLeftPosi() < 0) {
//        if ((getLeftPosi() + 200) < 0) {
////            console.log("1-")
//            $('.list').animate({left: "+=" + 200 + "px"}, 'slow', function () {
//
//            });
//        } else {
////            console.log("2-")
//            $('.list').animate({left: "+=" + ((getLeftPosi() * -1) + 10) + "px"}, 'slow', function () {
//
//            });
//
//
//        }
//    }
//}
//
//function scrolllefth() {
////  
////	$('.scroller-right').fadeIn('slow');
////	$('.scroller-left').fadeOut('slow');
////    console.log(":::::");
////    console.log(getLeftPosi());
////    console.log(getRigthPosi());
////    console.log(widthOfList());
////    console.log(widthOfHidden());
////    console.log(":::::");
//    if (getRigthPosi() > widthOfHidden()) {
//        if ((getRigthPosi() - 200) > widthOfHidden()) {
////
////        currentPosition -= 200;
//
//            $('.list').animate({left: "-=" + 200 + "px"}, 'slow', function () {
//
//            });
//        } else {
//            $('.list').animate({left: "-=" + ((getRigthPosi() - widthOfHidden()) + 10) + "px"}, 'slow', function () {
//
//            });
//
//
//        }
//    }
//}

function validador() {
    //alert("validador");   
    validator = $("#FormAplication").validate()({
        rules: {
        },
        messages: {
        },
        submitHandler: function () {

            var cadenaFormulario = $('#FormAplication').serialize();
            var request = $('#FormAplication #request').val();
            var funcion = $('#FormAplication #accion').val();
            var texto = "";
            alert(request);
            alert(cadenaFormulario);
            alerta("Mensaje del Sistema", "espera", "dialog");
            $.ajax({
                url: request,
                type: "POST",
                data: cadenaFormulario,
//                contentType: "application/x-www-form-urlencoded;charset=ISO-8859-15",
//                contentType: "application/x-www-form-urlencoded;charset=UTF-8",
//                 contentType: false,
//                processData: false,
                success: function (r) {
                    eval(r);
                },
                timeout: 30000,
                error: function () {
                    alertify.alert("Error!!", "Error en el procesamiento");
                }
            });
        },
        success: function (label) {

        }
    });
}

function direccionarlink(url) {
    if (url != "") {
        location.href = url;
    }
}

function RegargarTag(id, url, tipo) {
    if (url != "") {
        $.ajax({
            url: url,
            type: "POST",
            data: "",
            success: function (datos) {
                //alert(url);
                if (tipo == 'select') {
                    $("#" + id).html("");
                    $("#" + id).append(datos);
                }
                if (tipo == 'table') {
                    $("#" + id).html("");
                    $("#" + id).append(datos);
                }
                if (tipo == 'div') {
                    $("#" + id).html(datos);
                }
            },
            error: function () {
                alertify.alert("Error!!", "Error en el procesamiento");
            }
        });
    }
}
